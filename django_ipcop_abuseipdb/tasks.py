from __future__ import absolute_import, unicode_literals

import csv
from io import StringIO
from itertools import chain

import requests
from celery import shared_task
from django.conf import settings
from requests import ConnectionError, ReadTimeout

from django_ipcop_models.django_ipcop_models.models import *


@shared_task(name="ipcop_abuseipdb_sync_abuses")
def ipcop_abuseipdb_sync_abuses():
    api_url = "https://api.abuseipdb.com/api/v2/bulk-report"
    output = StringIO()

    abuses_no_sync_ip4 = IpcopAbuseIp4.objects.exclude(sync_partners_out=1).order_by(
        "id"
    )
    abuses_no_sync_ip6 = IpcopAbuseIp6.objects.exclude(sync_partners_out=1).order_by(
        "id"
    )
    abuses_no_sync = list(chain(abuses_no_sync_ip4, abuses_no_sync_ip6))[0:100]

    if len(abuses_no_sync) < 1:
        return

    fieldnames = ["IP", "Categories", "ReportDate", "Comment"]
    csvwriter = csv.DictWriter(output, fieldnames=fieldnames, dialect="unix")
    csvwriter.writeheader()

    for abuse in abuses_no_sync:
        all_cats = list()

        for category in abuse.categories.all():
            all_cats.append(str(category.id))

        category_txt = ",".join(all_cats)

        csvwriter.writerow(
            {
                "IP": str(abuse.ip.ipaddr),
                "Categories": category_txt,
                "ReportDate": abuse.report_received.isoformat(),
                "Comment": abuse.comment[:1023],
            }
        )

    try:
        r = requests.post(
            api_url,
            files={"csv": output.getvalue()},
            headers={"Key": settings.IPCOP_ABUSEIPDB_API_KEY},
            timeout=20,
        )
    except (ConnectionError, ReadTimeout):
        # Ignore connection errors and wait for next run
        return

    if r.status_code == 200 or r.status_code == 201:
        for abuse in abuses_no_sync:
            abuse.sync_partners_out.add("1")
            abuse.save()

    return
